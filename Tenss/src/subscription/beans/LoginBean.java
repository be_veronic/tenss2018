package subscription.beans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import utils.Util;
import dao.UserDAO;

@ManagedBean(name = "loginBean")
@SessionScoped
/**
 *
 * @author User
 */
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String password;
	private String message, uname;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String loginProject() {
		boolean result;
		try {
			result = UserDAO.login(uname, password);

			if (result) {
				// get Http Session and store username
				HttpSession session = Util.getSession();
				session.setAttribute("username", uname);
			
				return "home?faces-redirect=true";
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Login!", "Please Try Again!"));
				return "login";
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"This user doesn't exist or is not active.", null));
			return "login";
		}
	}

	public String logout() {
		HttpSession session = Util.getSession();
		session.invalidate();
		return "login?faces-redirect=true";
	}

	public String changePassword(String password) {
		System.out.println("change password.......-------");
		UserDAO.updatePassword(Util.getUserName(), password);
		Util.getSession().invalidate();
		return "login?faces-redirect=true";
	}

	public void submit() {
		System.out.println("submit......");
	}
}