package subscription.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import dao.StudentDAO;
import model.Application;
import model.Student;

@ManagedBean(name = "studentBean")
@SessionScoped
public class StudentBean implements Serializable {
	

	private Student student;

	public StudentBean() {
		student = StudentDAO.getCurrentStudent();
	}
	
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Student getCurrentStudent() {
		student = StudentDAO.getCurrentStudent();
		return student;
	}

	public List<Application> getCurrentStudentApps() {
		return StudentDAO.getCurrentStudent().getApps();
	}

	public List<Student> getAllStudents() {
		return StudentDAO.getAllStudents();
	}
	
	public List<Application> getAllApplications() {
		return StudentDAO.getAllApplications();
	}
}
