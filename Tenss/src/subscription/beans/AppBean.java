package subscription.beans;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.StudentDAO;
import dao.UserDAO;
import model.Application;
import model.Student;
import model.UploadFile;
import utils.HibernateUtil;

@ManagedBean(name = "appBean")
@SessionScoped
public class AppBean {
	static Logger logger = LoggerFactory.getLogger(AppBean.class);

	private Application application;

	@PostConstruct
	public void init() {
		application = StudentDAO.getApplicationStudent();
	}

	public List<UploadFile> uploadedFiles() {
		try {
			return StudentDAO.getUploadsStudent();
		} catch (Exception e) {
			return null;
		}
	}

	public void submitApp(Student stud, UploadBean ubean) {

		org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
		Timestamp submissionDate = new java.sql.Timestamp(new Date().getTime());
		try {
			session.beginTransaction();
			List<String> typeList = new ArrayList<>();
			List<String> necessaryTypes = new ArrayList<>();
			List<UploadFile> filesUploaded = StudentDAO.getUploadsStudent();

			ubean.setUploadFiles(filesUploaded);
			stud.setUploadedFiles(filesUploaded);
			if (filesUploaded.size() >= 4) {
				for (UploadFile u : filesUploaded) {
					typeList.add(u.getFileType());
				}
				necessaryTypes.add("CV");
				necessaryTypes.add("motivation letter");
				necessaryTypes.add("recommendation");
				necessaryTypes.add("recommendation");
			}
			if (filesUploaded.size() < 4 || Collections.frequency(typeList, "recommendation") < 2
					|| !typeList.containsAll(necessaryTypes)) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"For submission your application should contain a CV, a motivation letter and 2 recommendations!",
								null));
				return;
			}

			if (application.getYear() != null) {

				Application app = new Application(application.getYear(), application.getEmail());
				app.setStudent(application.getStudent());
				app.setTitle(application.getTitle());
				app.setCitizenship(application.getCitizenship());
				app.setVisa(application.isVisa());
				app.setImmigrationOffice(application.isImmigrationOffice());
				app.setDiet(application.getDiet());
				app.setFinancialSupport(application.isFinancialSupport());
				app.setReasonFinancialSupport(application.getReasonFinancialSupport());
				app.setInstitution(application.getInstitution());
				app.setLab_name(application.getLab_name());
				app.setDepartment(application.getDepartment());
				app.setStreet(application.getStreet());
				app.setPosition(application.getPosition());
				app.setPostal_code(application.getPostal_code());
				app.setCountry(application.getCountry());
				app.setCity(application.getCity());
				app.setPhone(application.getPhone());
				app.setFax(application.getFax());
				app.setSubmitted(true);
				app.setLastUpdated(submissionDate);
				session.update(app);

			} else {
				application.setYear(new java.sql.Timestamp(new Date().getTime()));
				application.setEmail(stud.getEmail());
				application.setStudent(stud);
				application.setSubmitted(true);
				application.setLastUpdated(submissionDate);
				session.save(application);
			}

			Student student = new Student(stud.getEmail());
			application.setLastUpdated(submissionDate);
			student.setFirstname(stud.getFirstname());
			student.setLastname(stud.getLastname());
			student.setGender(stud.getGender());
			for (UploadFile ufile : filesUploaded) {
				UploadFile u = new UploadFile(ufile.getFileID());
				u.setContentType(ufile.getContentType());
				u.setFile(ufile.getFile());
				u.setFileName(ufile.getFileName());
				u.setFileType(ufile.getFileType());
				u.setStudent(ufile.getStudent());
				u.setSubmittedBy(ufile.getSubmittedBy());
				session.update(u);
			}

			session.update(student);
			student.getUploadedFiles();
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
		} finally {
			if (session != null) {
				session.close();
			}
		}
		String submittDate = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(submissionDate);
		UserDAO.composeApplicationSubmittedEmail(stud.getEmail(), submittDate);
	}

	public void updateFileType(List<UploadFile> filesUploaded) {
		org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		for (UploadFile ufile : filesUploaded) {
			UploadFile u = new UploadFile(ufile.getFileID());
			u.setContentType(ufile.getContentType());
			u.setFile(ufile.getFile());
			u.setFileName(ufile.getFileName());
			u.setFileType(ufile.getFileType());
			u.setStudent(ufile.getStudent());
			u.setSubmittedBy(ufile.getSubmittedBy());
			session.update(u);
		}
		session.getTransaction().commit();
		session.close();
	}

	public void saveApp(Student stud, List<UploadFile> filesUploaded) {

		org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			if (application.getYear() != null) {

				Application app = new Application(application.getYear(), application.getEmail());
				/*
				 * app.setLastUpdated(java.sql.Timestamp.valueOf(new
				 * Date().toString() ));
				 */
				app.setStudent(application.getStudent());
				app.setTitle(application.getTitle());
				app.setCitizenship(application.getCitizenship());
				app.setVisa(application.isVisa());
				app.setImmigrationOffice(application.isImmigrationOffice());
				app.setDiet(application.getDiet());
				app.setFinancialSupport(application.isFinancialSupport());
				app.setReasonFinancialSupport(application.getReasonFinancialSupport());
				app.setInstitution(application.getInstitution());
				app.setLab_name(application.getLab_name());
				app.setDepartment(application.getDepartment());
				app.setStreet(application.getStreet());
				app.setPosition(application.getPosition());
				app.setPostal_code(application.getPostal_code());
				app.setCountry(application.getCountry());
				app.setCity(application.getCity());
				app.setPhone(application.getPhone());
				app.setFax(application.getFax());
				app.setSubmitted(application.isSubmitted());

				session.update(app);

			} else {
				application.setFinancialSupport(this.getApplication().isFinancialSupport());
				application.setReasonFinancialSupport(this.getApplication().getReasonFinancialSupport());
				application.setYear(new java.sql.Timestamp(new Date().getTime()));
				application.setEmail(stud.getEmail());
				application.setStudent(stud);
				application.setSubmitted(false);
				session.save(application);
			}
			// application.setLastUpdated(java.sql.Timestamp.valueOf(new
			// Date().toString()));
			Student student = new Student(stud.getEmail());

			student.setFirstname(stud.getFirstname());
			student.setLastname(stud.getLastname());
			student.setGender(stud.getGender());
			for (UploadFile ufile : filesUploaded) {
				UploadFile u = new UploadFile(ufile.getFileID());
				u.setContentType(ufile.getContentType());
				u.setFile(ufile.getFile());
				u.setFileName(ufile.getFileName());
				u.setFileType(ufile.getFileType());
				u.setStudent(ufile.getStudent());
				u.setSubmittedBy(ufile.getSubmittedBy());
				session.update(u);
			}

			session.update(student);
			session.getTransaction().commit();

		} catch (RuntimeException e) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public String displayDate(java.sql.Timestamp year) {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			return formatter.format(year);
		} catch (Exception e) {
			//
		}
		return "First submission";
	}

}
