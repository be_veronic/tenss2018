package subscription.beans;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.mail.MessagingException;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.StudentDAO;
import dao.UserDAO;
import utils.EncryptUtils;
import utils.HibernateUtil;

@ManagedBean(name = "registrationBean")
@RequestScoped
public class RegistrationBean {

	static Logger logger = LoggerFactory.getLogger(RegistrationBean.class);
	private RegistrationForm registrationForm = null;
	private List<String> genders = null;

	public RegistrationBean() {

		this.genders = new ArrayList<String>();
		this.genders.add("Male");
		this.genders.add("Female");

	}

	public String register() {
		System.out.println("register.....");
		// store data in DB

		try {
			UserDAO.register(registrationForm.getEmail(), registrationForm.getPassword(),
					registrationForm.getFirstName(), registrationForm.getLastName(), registrationForm.getGender());
		} catch (HibernateException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					e.getMessage(), null));

			return null;	
		} catch (SQLException e) {
			logger.error(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
			return null;
		}

		try {
			UserDAO.composeActivationEmail(EncryptUtils.encrypt(registrationForm.getEmail()));
			FacesMessage fm = new FacesMessage("Please check your inbox and spam folders for the activation link.");
			FacesContext.getCurrentInstance().addMessage(null, fm);
		} catch (UnsupportedEncodingException | MessagingException | GeneralSecurityException e) {
			logger.error(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"We can't send you the activation link by email! Please let us know by writing an email to contact@tenss.ro.", null));
		    StudentDAO.deleteStudent(registrationForm.getEmail());
			UserDAO.deleteUser(registrationForm.getEmail());
		}
		return "login";
	}

	public RegistrationForm getRegistrationForm() {
		if (this.registrationForm == null) {
			this.registrationForm = new RegistrationForm();
		}
		return registrationForm;
	}

	public void setRegistrationForm(RegistrationForm registrationForm) {
		this.registrationForm = registrationForm;
	}

	public List<String> getGenders() {
		return genders;
	}

	public void setGenders(List<String> genders) {
		this.genders = genders;
	}

}