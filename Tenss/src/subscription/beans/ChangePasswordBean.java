package subscription.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

@javax.faces.bean.SessionScoped
@ManagedBean(name = "passwordManager")
public class ChangePasswordBean implements Serializable {

   
	private String password;
	private String passwordconfirm;
   
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordconfirm() {
		return passwordconfirm;
	}

	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}

	
}



