package subscription.beans;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.mail.MessagingException;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.StudentDAO;
import dao.UserDAO;
import model.UploadFile;
import utils.HibernateUtil;
import utils.Util;

@ManagedBean(name = "fileUploadView")
@RequestScoped
public class FileUploadView {

	static Logger logger = LoggerFactory.getLogger(FileUploadView.class);

	@ManagedProperty(value = "#{param.email}")
	private String email;

	String id;
	private UploadFile ufile = new UploadFile();

	private UploadBean uBean;

	private String profName;

	@PostConstruct
	public void init() {
		uBean = new UploadBean(email);
	}

	public void handleFileUpload(FileUploadEvent event) {
		if (!PhaseId.INVOKE_APPLICATION.equals(event.getPhaseId())) {
			event.setPhaseId(PhaseId.INVOKE_APPLICATION);
			event.queue();
		} else {

			if (StudentDAO.getCurrentStudent() == null && StudentDAO.getStudent(email) == null) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"This applicant does not exist. Please ensure the applicant's email address is correct.",
								null));
				return;
			}

			org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();

			getUfile().setFileName(event.getFile().getFileName());
			getUfile().setFileName(FilenameUtils.getName(event.getFile().getFileName()));
			getUfile().setContentType(event.getFile().getContentType());
			getUfile().setFileType("CV");

			try {
				if (Util.getUserName() != null) {
					getUfile().setStudent(StudentDAO.getCurrentStudent());
					if (email != null) {
						getUfile().setFileType("recommendation");
					}
				}
			} catch (NullPointerException e) {
				getUfile().setStudent(StudentDAO.getStudent(email));
				getUfile().setFileType("recommendation");
			}

			try {

				byte[] blob = IOUtils.toByteArray(event.getFile().getInputstream());

				getUfile().setFile(blob);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (profName == null) {
				getUfile().setSubmittedBy("user");
			} else {
				getUfile().setSubmittedBy(profName);
			}
			getuBean().getUploadFiles().add(getUfile());
			try {
				session.save(getUfile());
				session.getTransaction().commit();
			} catch (RuntimeException e) {
				try {
					session.getTransaction().rollback();
				} catch (RuntimeException rbe) {
					logger.error("Couldn�t roll back transaction", rbe);
				}
				throw e;
			} finally {
				if (session != null) {
					session.close();
				}
			}
			if (email != null)
				try {
					UserDAO.composeUploadNotificationEmail(email);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Thank you! A notification email was sent to the applicant.", null));
				} catch (MessagingException e) {
					logger.error(e.getMessage());
				}
			
		}

	}

	public void deleteFile(UploadFile file) {
		org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.delete(file);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfName() {
		return profName;
	}

	public void setProfName(String profName) {
		this.profName = profName;
	}

	public UploadBean getuBean() {
		return uBean;
	}

	public void setuBean(UploadBean uBean) {
		this.uBean = uBean;
	}

	public UploadFile getUfile() {
		return ufile;
	}

	public void setUfile(UploadFile ufile) {
		this.ufile = ufile;
	}

}
