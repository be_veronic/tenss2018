package subscription.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.hibernate.Query;

import model.UploadFile;
import utils.HibernateUtil;
import utils.Util;

@ManagedBean(name = "uploadBean")
@SessionScoped
public class UploadBean implements Serializable {

	private List<UploadFile> uploadFiles;

	public UploadBean(String email) {
		if (email == null)
			uploadFiles = getStudentUploads(Util.getUserName());
		else uploadFiles = getStudentUploads(email);
	}

	public List<UploadFile> getStudentUploads(String email) {
		org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();

		Query query = session.createQuery("from UploadFile where student_email=:email");
		query.setParameter("email", email);
		List<UploadFile> list = query.list();
		return list;
	}

	public List<UploadFile> getUploadFiles() {
		return uploadFiles;
	}

	public void setUploadFiles(List<UploadFile> uploadFiles) {
		this.uploadFiles = uploadFiles;
	}

	



}