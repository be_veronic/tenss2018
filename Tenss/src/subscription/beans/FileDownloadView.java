package subscription.beans;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.faces.bean.ManagedBean;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
 
@ManagedBean
public class FileDownloadView {
     
    private StreamedContent file;
    
     
    public void handleFileDownload(byte[] blob, String contentType, String fileName) {        
     
        setFile(new DefaultStreamedContent(new ByteArrayInputStream(blob), contentType, fileName));    
    }
 
    public StreamedContent getFile() {
        return file;
    }

	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
}
