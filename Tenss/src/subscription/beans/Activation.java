package subscription.beans;

import java.io.IOException;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.UserDAO;
import utils.EncryptUtils;

@ManagedBean(name = "activation")
@RequestScoped
public class Activation {

    @ManagedProperty(value="#{param.key}")
    private String key;
	static Logger logger = LoggerFactory.getLogger(Activation.class);

    public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	private boolean valid;

    @PostConstruct
    public void init() {
    	System.out.println("activaaaaaate");
    	System.out.println(key);
    	try {
			UserDAO.updateActivation(URLDecoder.decode(EncryptUtils.decrypt(key), "UTF-8"));
			valid = true;
		} catch (GeneralSecurityException | IOException e) {
			logger.debug(e.getMessage());
		}
    }

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

  
}