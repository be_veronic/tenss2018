package subscription.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "pages")
@SessionScoped
public class Pages {

	private String dynamicaPagesInclude = "info.xhtml";

	public String getDynamicaPagesInclude() {
		return dynamicaPagesInclude;
	}

	public void setDynamicaPagesInclude(String dynamicaPagesInclude) {
		this.dynamicaPagesInclude = dynamicaPagesInclude;
	}

	public String changePage(int itemSelected) {
		if (itemSelected == 1) {
			dynamicaPagesInclude = "applyTenss.xhtml";
		} else if (itemSelected == 2) {
			dynamicaPagesInclude = "myApplications.xhtml";
		} else if (itemSelected == 3) {
			dynamicaPagesInclude = "changePassword.xhtml";
		} else if (itemSelected == 4) {
			dynamicaPagesInclude = "allApplications.xhtml";
		}
		else {
			dynamicaPagesInclude = "info.xhtml";
		}
		return dynamicaPagesInclude;
	}

	public void resetPage() {
		this.dynamicaPagesInclude = null;
	}

}