package subscription.beans;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.UserDAO;

@javax.faces.bean.SessionScoped
@ManagedBean(name = "ForgotPassword")
public class ForgotPasswordBean implements Serializable {
	
	static Logger logger = LoggerFactory.getLogger(ForgotPasswordBean.class);
	private SessionIdentifierGenerator gen = new SessionIdentifierGenerator();
	private String email;

	public String apply() {
		String newPw = gen.nextSessionId();
		boolean existsUser = UserDAO.emailExists(email);		
		if (existsUser) {
			final Properties props = new Properties();

			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
					.getContext();
			try {
				props.load(servletContext.getResourceAsStream("/WEB-INF/mail_server.properties"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(props.getProperty("mail.smtp.user"),
							props.getProperty("mail.password"));
				}
			});
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(props.getProperty("mail.from")));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));

				message.setSubject("TENSS: recovery password email");

				// user.setPassword(newPw);
				if (true) {
					message.setContent("Dear applicant, <BR><BR>  You requested a reset of your password."
							+ "Your new password is: " + newPw + "<BR><BR> Best regards, <BR>TENSS organizing committee <BR><BR>Please do not reply to this email. If you have questions, please write to contact@tenss.ro.", "text/html");
					Transport transport = session.getTransport("smtp");
					try {
						transport.send(message);
					} catch (Exception e) {
						logger.error("Mail server is down!!!");
						FacesContext.getCurrentInstance().addMessage(null,
								new FacesMessage(FacesMessage.SEVERITY_ERROR, "We can't send you the new password. Please let us know by writing an email to contact@tenss.ro.", null));
						
					}
					transport.close();
					System.out.println("Sent message successfully....");
					UserDAO.updatePassword(email, newPw);
				}
				FacesMessage fm = new FacesMessage("Please check your inbox and spam folders for the new password.");
				FacesContext.getCurrentInstance().addMessage(null, fm);
			} catch (MessagingException mex) {
				logger.error("Mail server is down!!!");
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "We can't send you the new password. Please let us know by writing an email to contact@tenss.ro.", null));
				
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_ERROR, "This email address doesn't exists.", null));
		}
		return "login.xhtml";// go to login.xhtml
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String emailen) {
		email = emailen;
	}
}

final class SessionIdentifierGenerator {
	private SecureRandom random = new SecureRandom();

	public String nextSessionId() {
		return new BigInteger(130, random).toString(32);
	}
}