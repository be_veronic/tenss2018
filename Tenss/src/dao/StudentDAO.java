package dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Application;
import model.Student;
import model.UploadFile;
import utils.HibernateUtil;
import utils.Util;

public class StudentDAO {

	static Logger logger = LoggerFactory.getLogger(StudentDAO.class);

	public static Student getCurrentStudent() {
		Student stud = null;
		org.hibernate.Session session = null;
		try {
			session = utils.HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			stud = (Student) session.load(Student.class, utils.Util.getUserName());
			Hibernate.initialize(stud);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return stud;
	}

	public static Application getApplicationStudent() {
		Student stud = null;
		org.hibernate.Session session = null;
		Application app = new Application();
		try {
			session = utils.HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			stud = (Student) session.load(Student.class, Util.getUserName());
			if (stud.getApps().size() > 0) {
				app = stud.getApps().get(0);
			}
			Hibernate.initialize(app);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return app;
	}

	public static List<UploadFile> getUploadsStudent() {
		Student stud = null;
		org.hibernate.Session session = null;
		Application app = new Application();
		try {
			session = utils.HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			stud = (Student) session.load(Student.class, Util.getUserName());
			if (stud.getUploadedFiles().size() > 0) {
				for (UploadFile uFile : stud.getUploadedFiles()) {
					Hibernate.initialize(uFile);
				}
			}
			session.getTransaction().commit();

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return stud.getUploadedFiles();
	}

	public static List<Student> getAllStudents() {
		org.hibernate.Session session = utils.HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Student");
		List<Student> list = query.list();
		return list;
	}

	public static List<Application> getAllApplications() {
		org.hibernate.Session session = utils.HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Application");
		List<Application> list = query.list();
		return list;
	}

	public static Student getStudent(String email) {
		Student stud = null;
		org.hibernate.Session session = null;
		try {
			session = utils.HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			stud = (Student) session.get(Student.class, email);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return stud;
	}

	public static void deleteStudent(String email) {
		org.hibernate.Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(new Student(email));
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}
}
