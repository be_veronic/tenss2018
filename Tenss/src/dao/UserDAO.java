package dao;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javassist.tools.rmi.ObjectNotFoundException;
import model.Gender;
import model.Student;
import model.Userinfo;
import utils.EncryptUtils;
import utils.HibernateUtil;

public class UserDAO {

	static Properties props = new Properties();

	static Logger logger = LoggerFactory.getLogger(UserDAO.class);

	static {

		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
				.getContext();
		try {
			props.load(servletContext.getResourceAsStream("/WEB-INF/mail_server.properties"));
		} catch (IOException e) {
			logger.error("could not load email properties");
		}
	}

	public static boolean login(String user, String pass) throws Exception {
		Userinfo userinfo = null;
		org.hibernate.Session session = null;

		session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			userinfo = (Userinfo) session.get(Userinfo.class, user);
			if (!userinfo.getUserpass().equals(EncryptUtils.encrypt(pass)) || !userinfo.isActive()) {
				return false;
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			throw e;

		} catch (RuntimeException e) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return true;
	}

	public static void register(String user, String password, String firstname, String lastname, String gender)
			throws SQLException {

		org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Userinfo userinfo = new Userinfo();
		userinfo.setUsername(user);
		try {
			userinfo.setUserpass(EncryptUtils.encrypt(password));
			userinfo.setVerification_key(EncryptUtils.encrypt(user));
		} catch (UnsupportedEncodingException | GeneralSecurityException e) {
			logger.error(e.getMessage());
		}

		Student stud = new Student();
		stud.setEmail(user);
		stud.setFirstname(firstname);
		stud.setLastname(lastname);
		stud.setGender(Gender.valueOf(gender));

		userinfo.setStudent(stud);
		stud.setUserinfo(userinfo);

		try {
			session.save(userinfo);
			session.save(stud);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			throw new HibernateException(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public static boolean emailExists(String user) {
		Userinfo userinfo = null;
		org.hibernate.Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		userinfo = (Userinfo) session.get(Userinfo.class, user);
		if (userinfo == null || !userinfo.isActive())
			return false;
		return true;
	}

	public static void deleteUser(String user) {
		org.hibernate.Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(new Userinfo(user));
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public static Userinfo getUser(String email) {
		Userinfo user = null;
		org.hibernate.Session session = null;
		try {
			session = utils.HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			user = (Userinfo) session.get(Userinfo.class, email);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return user;
	}

	public static void updatePassword(String user, String pass) {
		Userinfo userinfo = null;
		org.hibernate.Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			userinfo = (Userinfo) session.load(Userinfo.class, user);
			userinfo.setUserpass(EncryptUtils.encrypt(pass));
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
			throw e;
		} catch (UnsupportedEncodingException e) {
			logger.debug(e.getMessage());
		} catch (GeneralSecurityException e) {
			logger.debug(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public static void updateActivation(String user) {
		Userinfo userinfo = null;
		org.hibernate.Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			userinfo = (Userinfo) session.load(Userinfo.class, user);
			Hibernate.initialize(user);
			userinfo.setActive(true);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public static void composeActivationEmail(String key) throws MessagingException {

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.smtp.user"),
						props.getProperty("mail.password"));
			}
		});
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(props.getProperty("mail.from")));
			try {
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(EncryptUtils.decrypt(key)));
			} catch (GeneralSecurityException | IOException e) {
				logger.error(e.getMessage());
			}

			message.setSubject("TENSS: activation email");
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			String url = request.getRequestURL().toString();
			String baseURL = url.substring(0, url.length() - request.getRequestURI().length())
					+ request.getContextPath() + "/";
			String link = baseURL + "/activation.jsf?key=" + URLEncoder.encode(key, "UTF-8");
			// user.setPassword(newPw);
			if (true) {
				message.setContent(
						"Dear applicant, <BR><BR> Please follow this link to activate your TENSS account: <BR> " + link
								+ "<BR><BR>If you have any difficulties creating the account or uploading your application, please contact us at: contact@tenss.ro. Do not reply to this email! <BR><BR> Best regards, <BR>TENSS organizing committee",
						"text/html");
				Transport transport = session.getTransport("smtp");
				transport.send(message);
				transport.close();
				System.out.println("Sent message successfully....");
				// UserDAO.updatePassword(email, newPw);
			}
		} catch (MessagingException mex) {
			throw mex;
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage());
		}
	}

	public static void composeUploadNotificationEmail(String key) throws MessagingException {

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.smtp.user"),
						props.getProperty("mail.password"));
			}
		});
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(props.getProperty("mail.from")));

			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(key));

			message.setSubject("TENSS: a recommendation was uploaded for you");
			if (true) {
				message.setContent(
						"Dear applicant, <BR><BR> A recommendation letter was uploaded for you. Please check the Upload section of your TENSS application. Please note that once it is complete you have to submit your application in order to be considered for evaluation!<BR><BR>Best regards, <BR>TENSS Organizing Committee <BR><BR>Please do not reply to this email. If you have questions, please write to contact@tenss.ro.",
						"text/html");
				Transport transport = session.getTransport("smtp");
				transport.send(message);
				transport.close();
				System.out.println("Sent message successfully....");
				// UserDAO.updatePassword(email, newPw);
			}
		} catch (MessagingException mex) {
			logger.error(mex.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"We can't send an email to the applicant due to an error. Please notify the applicant yourself that you have uploaded a recommendation for her/him.", null));
			throw mex;
		}
	}

	public static void composeApplicationSubmittedEmail(String email, String date) {
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.smtp.user"),
						props.getProperty("mail.password"));
			}
		});
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(props.getProperty("mail.from")));

			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));

			message.setSubject("TENSS: application successfully submitted");
			if (true) {
				message.setContent("Dear applicant, <BR><BR> Your application was sucessfully submitted on " + date
						+ ".<BR><BR>Best regards, <BR>TENSS Organizing Committee <BR><BR>Please do not reply to this email. If you have questions, please write to contact@tenss.ro.", "text/html");
				Transport transport = session.getTransport("smtp");
				transport.send(message);
				transport.close();
				System.out.println("Sent message successfully....");
				// UserDAO.updatePassword(email, newPw);
			}
			FacesMessage fm = new FacesMessage("The confirmation of your submission was sent by email.");
			FacesContext.getCurrentInstance().addMessage(null, fm);
		} catch (MessagingException mex) {
			logger.error(mex.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Your application is submitted. Unfortunately we can't send you the email with the confirmation for your submission! Please let us know by writing an email to contact@tenss.ro.",
							null));
		}
	}

	public static void composeUploadRecommandationsEmail(Student stud, String profName, String profEmail) {

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.smtp.user"),
						props.getProperty("mail.password"));
			}
		});
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(props.getProperty("mail.from")));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(profEmail));

			message.setSubject("TENSS: upload recommandation");
			String link = "";
			FacesContext context = FacesContext.getCurrentInstance();
			link = (ServletContext) context.getCurrentInstance().getExternalContext().getContext()
					+ "/uploadRecommandation.jsf?email=" + utils.Util.getUserName();
			// user.setPassword(newPw);
			if (true) {
				message.setContent("Dear Professor " + profName + ",<BR><BR> please follow the link ,<BR>" + link
						+ ",<BR> to upload a recommandation letter for student " + stud.getFirstname() + " "
						+ stud.getLastname() + ".<BR><BR><BR>Best regards, TENSS", "text/html");
				Transport transport = session.getTransport("smtp");
				transport.send(message);
				transport.close();
				System.out.println("Sent message successfully....");
				// UserDAO.updatePassword(email, newPw);
			}
			FacesMessage fm = new FacesMessage("Email sent");
			FacesContext.getCurrentInstance().addMessage(null, fm);
		} catch (MessagingException mex) {
			logger.error(mex.getMessage());
		}
	}
}