package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "student")  
public class Student implements Serializable{
	
	public Student(String firstname, String lastname, Gender gender) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.gender = gender;
	}

	public Student() {
		// TODO Auto-generated constructor stub
	}
	
	public Student(String email) {
		this.email = email;
	}

	private String firstname;
	
	private String lastname;
	
	@OneToOne(fetch = FetchType.LAZY)
	private Userinfo userinfo;
	
	@OneToMany(mappedBy="student", fetch=FetchType.LAZY)
	private List<Application> apps;
	
	@OneToMany(mappedBy="student", fetch=FetchType.LAZY)
	private List<UploadFile> uploadedFiles;
	
	@Id
	@Column(name="email")
	private String email;
	
	
	
	@Enumerated(EnumType.STRING)
	private Gender gender;



	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Userinfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public List<Application> getApps() {
		return apps;
	}

	public void setApps(List<Application> apps) {
		this.apps = apps;
	}

	public List<UploadFile> getUploadedFiles() {
		return uploadedFiles;
	}

	public void setUploadedFiles(List<UploadFile> uploadedFiles) {
		this.uploadedFiles = uploadedFiles;
	}
	
	
	
}
