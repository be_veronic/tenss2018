package model;

import org.hibernate.Session;

import utils.HibernateUtil;

public class MainClass {
	public static void main(String[] args) {
		System.out.println("Hibernate one to one (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Userinfo user = new Userinfo();

		user.setUsername("test@test.ro");

		Student stud = new Student();
		stud.setEmail("test@test.ro");
		user.setStudent(stud);
		stud.setUserinfo(user);

		

		session.save(user);
		session.getTransaction().commit();

		System.out.println("Done");
	}
}