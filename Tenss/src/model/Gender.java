package model;

public enum Gender {
	Male("Male"), Female("Female");
	private final String label;

	private Gender(String label) {
		this.label = label;
	}

	public String getLabel() {
	    return this.label;
	}
}
