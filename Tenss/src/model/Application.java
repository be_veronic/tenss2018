package model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "application")

public class Application implements Serializable {

	public Application() {

	}

	public Application(Timestamp year2, String email2) {
		this.year = year2;
		this.email = email2;
	}

	public Application(Timestamp year2, String email2, String citizenship2) {
		this.year = year2;
		this.email = email2;
		this.citizenship = citizenship2;
	}


	@Id
	@Column(name = "email")
	private String email;

	@Id
	private java.sql.Timestamp year;
	
	private java.sql.Timestamp lastUpdated;

	private String title;

	private String citizenship;

	@Column(columnDefinition = "bit default 0")
	private boolean visa;

	private String institution;
	private String department;
	private String position;
	private String lab_name;
	private String street;
	private String postal_code;
	private String city;
	private String country;
	private String phone;
	private String fax;
	@Column(columnDefinition = "bit default 0")
	private boolean submitted;
	

	public boolean isVisa() {
		return visa;
	}

	public void setVisa(boolean visa) {
		this.visa = visa;
	}

	public boolean isImmigrationOffice() {
		return immigrationOffice;
	}

	public void setImmigrationOffice(boolean immigrationOffice) {
		this.immigrationOffice = immigrationOffice;
	}

	public boolean isFinancialSupport() {
		return financialSupport;
	}

	public void setFinancialSupport(boolean financialSupport) {
		this.financialSupport = financialSupport;
	}

	@Column(columnDefinition = "bit default 0")
	private boolean immigrationOffice;

	private String diet;

	@Column(columnDefinition = "bit default 0")
	private boolean financialSupport;
	
	private String reasonFinancialSupport;

	@ManyToOne
	private Student student;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public java.sql.Timestamp getYear() {
		return year;
	}

	public void setYear(java.sql.Timestamp date) {
		this.year = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getDiet() {
		return diet;
	}

	public void setDiet(String diet) {
		this.diet = diet;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getLab_name() {
		return lab_name;
	}

	public void setLab_name(String lab_name) {
		this.lab_name = lab_name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public java.sql.Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(java.sql.Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getPostal_code() {
		return postal_code;
	}

	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getReasonFinancialSupport() {
		return reasonFinancialSupport;
	}

	public void setReasonFinancialSupport(String reasonFinancialSupport) {
		this.reasonFinancialSupport = reasonFinancialSupport;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

}
