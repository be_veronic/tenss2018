package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import model.Student;
@Entity
@Table(name = "userinfo")  
public class Userinfo {
	public Userinfo(String username, String userpass, String verification_key, Student student, boolean active) {
		super();
		this.username = username;
		this.userpass = userpass;
		this.verification_key = verification_key;
		this.student = student;
		this.setActive(active);
	}
	public Userinfo() {
		// TODO Auto-generated constructor stub
	}
	
	public Userinfo(String username) {
		this.username = username;
	}
	
	@Id
	private String username;
	private String userpass;
	private String verification_key;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "userinfo", cascade = CascadeType.ALL)
	private Student student;
	
	
	@Column(columnDefinition = "bit default 0")
	private boolean active;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserpass() {
		return userpass;
	}
	public void setUserpass(String userpass) {
		this.userpass = userpass;
	}
	public String getVerification_key() {
		return verification_key;
	}
	public void setVerification_key(String verification_key) {
		this.verification_key = verification_key;
	}

	
	
	public Student getStudent() {
		return student;
	}
	
	public void setStudent(Student student) {
		this.student = student;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}
