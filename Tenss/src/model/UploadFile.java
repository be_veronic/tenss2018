package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.UserDAO;

@Entity
@Table(name = "upload_file")
public class UploadFile {
	
	static Logger logger = LoggerFactory.getLogger(UploadFile.class);
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int fileID;
	private String fileName;
	private String fileType;
	private String contentType;

	
	private byte[] file;

	@ManyToOne
	private Student student;
	
	private String submittedBy;
	
	public UploadFile() {
		
	}

	public UploadFile(int fileID2) {
		fileID = fileID2;
	}

	public int getFileID() {
		return fileID;
	}

	public void setFileID(int fileID) {
		this.fileID = fileID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
		Session session = utils.HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.update(this);
			session.getTransaction().commit();
		} catch (RuntimeException er) {
			try {
				session.getTransaction().rollback();
			} catch (RuntimeException rbe) {
				logger.error("Couldn�t roll back transaction", rbe);
			}
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}
}
