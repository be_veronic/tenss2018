package utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {
    private static SessionFactory sessionFactory ;
    static {
        Configuration configuration = new Configuration();

       
        configuration.setProperty("hibernate.connection.driver_class","org.postgresql.Driver");
        configuration.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/tenssdb2017");                                
        configuration.setProperty("hibernate.connection.username", "tenss_user");     
        configuration.setProperty("hibernate.connection.password", "11Sohodol");
        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        configuration.setProperty("hibernate.hbm2ddl.auto", "update");
        configuration.setProperty("show_sql", "true");
        configuration.addAnnotatedClass(model.Student.class);
        configuration.addAnnotatedClass(model.Userinfo.class);
        configuration.addAnnotatedClass(model.Application.class);
        configuration.addAnnotatedClass(model.UploadFile.class);

        ServiceRegistryBuilder builder = new ServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.buildServiceRegistry());
    }
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
} 
