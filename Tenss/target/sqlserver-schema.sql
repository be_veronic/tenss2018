
    alter table application 
        drop constraint FK_2sf943f794kt4jxlo8wsm7fx6;

    alter table student 
        drop constraint FK_949n5l0nck2nndelfdgmcpt09;

    alter table upload_file 
        drop constraint FK_tpl035g8w0h2nffh2fucqlfgp;

    drop table application;

    drop table student;

    drop table upload_file;

    drop table userinfo;

    create table application (
        year datetime not null,
        email varchar(255) not null,
        citizenship varchar(255),
        city varchar(255),
        country varchar(255),
        department varchar(255),
        diet varchar(255),
        fax varchar(255),
        financialSupport bit default 0,
        immigrationOffice bit default 0,
        institution varchar(255),
        lab_name varchar(255),
        lastUpdated datetime,
        phone varchar(255),
        position varchar(255),
        postal_code varchar(255),
        reasonFinancialSupport varchar(255),
        street varchar(255),
        submitted bit default 0,
        title varchar(255),
        visa bit default 0,
        student_email varchar(255),
        primary key (year, email)
    );

    create table student (
        email varchar(255) not null,
        firstname varchar(255),
        gender varchar(255),
        lastname varchar(255),
        userinfo_username varchar(255),
        primary key (email)
    );

    create table upload_file (
        fileID int identity not null,
        contentType varchar(255),
        file varbinary(255),
        fileName varchar(255),
        fileType varchar(255),
        submittedBy varchar(255),
        student_email varchar(255),
        primary key (fileID)
    );

    create table userinfo (
        username varchar(255) not null,
        active bit default 0,
        userpass varchar(255),
        verification_key varchar(255),
        primary key (username)
    );

    alter table application 
        add constraint FK_2sf943f794kt4jxlo8wsm7fx6 
        foreign key (student_email) 
        references student;

    alter table student 
        add constraint FK_949n5l0nck2nndelfdgmcpt09 
        foreign key (userinfo_username) 
        references userinfo;

    alter table upload_file 
        add constraint FK_tpl035g8w0h2nffh2fucqlfgp 
        foreign key (student_email) 
        references student;
